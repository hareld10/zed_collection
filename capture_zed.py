import cv2
import numpy as np
import pyzed.sl as sl
import time
from utils import *
import os


base = "/media/wisense/wiseData1/zed_recordings/"
# base = "./"
os.makedirs(base, exist_ok=True)
max_depth = 40

path_output = "./rgb_depth.svo"

def get_frame(cam, runtime):
    rgb_mat = sl.Mat()
    depth_mat = sl.Mat()
    depth_zed = sl.Mat(cam.get_resolution().width, cam.get_resolution().height, sl.MAT_TYPE.MAT_TYPE_32F_C1)
    point_cloud = sl.Mat()

    err = cam.grab(runtime)
    if err == sl.ERROR_CODE.SUCCESS:
        # retrieve rgb and depth
        cam.retrieve_image(rgb_mat, sl.VIEW.VIEW_LEFT, sl.MEM.MEM_CPU)
        cam.retrieve_image(depth_mat, sl.VIEW.VIEW_DEPTH, sl.MEM.MEM_CPU)
        cam.retrieve_measure(depth_zed, sl.MEASURE.MEASURE_DEPTH)
        cam.retrieve_measure(point_cloud, sl.MEASURE.MEASURE_XYZRGBA)

        image_ocv = rgb_mat.get_data()
        depth_image_ocv = depth_mat.get_data()
        depth_raw = depth_zed.get_data()
        pc_raw = point_cloud.get_data()
        return image_ocv, depth_image_ocv, depth_raw, pc_raw

    else:
        return None, None


def main():
    cam, runtime = run_zed_camera()

    name = str(time.time())
    path_offset = base + "depth_sync_" + name + ".csv"
    frames_count = 0
    path_output = base + "/rgb_depth_" + name + ".svo"
    cam.enable_recording(path_output, sl.SVO_COMPRESSION_MODE.SVO_COMPRESSION_MODE_LOSSLESS)

    try:
        with open(path_offset, "w") as sync_file:
            sync_file.write("timestampDepth\n")
            while True:
                try:
                    rgb, depth, depth_raw, pc = get_frame(cam, runtime)
                    if rgb is None:
                        continue
                    sbs_image = np.concatenate((rgb, depth), axis=1)
                    cv2.imshow("Image", cv2.resize(sbs_image, None,  fx=1/scale, fy=1/scale))

                    print(np.max(eliminate_values(depth_raw)))
                    print(pc.shape)
                    # if frames_count % 100 == 0:
                    #     path_output = base + "/rgb_depth_" + str(frames_count) + ".svo"
                    #     cam.enable_recording(path_output, sl.SVO_COMPRESSION_MODE.SVO_COMPRESSION_MODE_LOSSLESS)
                    cam.record()
                    sync_file.write(str("%.9f" % time.time()).replace(".", "") + "\n")

                    key = cv2.waitKey(10)
                    frames_count +=1

                except Exception as e:
                    time.sleep(0.01)
                    continue
                if key == ord('q'):
                    break

    except Exception as e:
        print(e)
        cam.close()
        cam.disable_recording()

    cam.disable_recording()
    cam.close()

if __name__ == '__main__':
    main()
