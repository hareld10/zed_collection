import pyzed.sl as sl

zed = sl.Camera()

init_params = sl.InitParameters()
init_params.depth_mode = sl.DEPTH_MODE.DEPTH_MODE_ULTRA # Use ULTRA depth mode
init_params.coordinate_units = sl.UNIT.UNIT_MILLIMETER # Use millimeter units (for depth measurements)

image = sl.Mat()
depth_map = sl.Mat()
runtime_parameters = sl.RuntimeParameters()
# Open the camera

err = zed.open(init_params)
if err != sl.ERROR_CODE.SUCCESS:
    print("Failed to oprn camera")
    exit(-1)


if zed.grab(runtime_parameters) ==  sl.ERROR_CODE.SUCCESS :
  # A new image and depth is available if grab() returns SUCCESS
  zed.retrieve_image(image, sl.VIEW.VIEW_LEFT) # Retrieve left image
  zed.retrieve_measure(depth_map, sl.MEASURE.MEASURE_DEPTH) # Retrieve depth
else:
    print("Grabbing Failed")

depth_for_display = sl.Mat()
im = zed.retrieve_image(depth_for_display, sl.VIEW.VIEW_DEPTH)

