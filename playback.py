"""
    Read SVO sample to read the video and the information of the camera. It can pick a frame of the svo and save it as
    a JPEG or PNG file. Depth map and Point Cloud can also be saved into files.
"""
import sys
import pyzed.sl as sl
import cv2
from utils import *
import os
import pandas as pd

def main():

    # if len(sys.argv) != 2:
    #     print("Please specify path to .svo file.")
    #     exit()
    #
    # filepath = sys.argv[1]
    # print("Reading SVO file: {0}".format(filepath))

    base = "/media/amper/ssd24/200120/EXP3_W36_M8_RF0/depthRawData/"
    sync_file = "/media/amper/ssd24/200120/Out/EXP3_W36_M8_RF0/depth_sync.csv"

    base = "./"

    all_ts = base + "depth_sync.csv"
    df = pd.read_csv(all_ts, index_col=None)
    timestamps = df.loc[:, 'timestampDepth'].to_numpy().astype(np.int)
    print(timestamps)

    sync_file = "./syncData.csv"
    if os.path.exists(sync_file):
        df = pd.read_csv(sync_file, index_col=None)
        indices = df.loc[:, 'inFileIdxDepth'].to_numpy().astype(np.int)
        print(indices)
    else:
        indices = np.arange(len(timestamps))

    rgb_path = base + "/zed_images/"
    depth_path = base + "/zed_images/"
    os.makedirs(rgb_path, exist_ok=True)
    os.makedirs(depth_path, exist_ok=True)

    init = getParams()
    init.set_from_svo_file(base + "rgb_depth.svo")

    cam, runtime = run_zed_camera()
    mat = sl.Mat()
    depth_zed = sl.Mat(cam.get_resolution().width, cam.get_resolution().height, sl.MAT_TYPE.MAT_TYPE_32F_C1)

    key = ''
    frame_count = 0
    while key != ord('q'):  # for 'q' key
        err = cam.grab(runtime)
        if err == sl.ERROR_CODE.SUCCESS:
            cam.retrieve_image(mat, sl.VIEW.VIEW_LEFT)

            if not frame_count in indices:
                frame_count+=1
                if frame_count > np.max(indices):
                    break
                continue

            print("processing frame ", frame_count)
            # cv2.imshow("ZED", cv2.resize(mat.get_data(), None, fx=1/scale, fy=1/scale))
            key = cv2.waitKey(10)

            frame_name = "/frame_" + str(timestamps[frame_count]) + "_" + str(frame_count)

            try:
                cv2.imwrite(rgb_path + frame_name + ".png", mat.get_data()[:,:,:3])
            except Exception as e:
                break

            cam.retrieve_measure(depth_zed, sl.MEASURE.MEASURE_DEPTH)
            depth_ocv = depth_zed.get_data().astype(np.float32)
            np.savez(depth_path + frame_name + "_depth", depth_map=depth_ocv)

            # Old fashion ZED
            # save_depth = sl.save_camera_depth_as(cam, sl.DEPTH_FORMAT.DEPTH_FORMAT_PNG, depth_path + frame_name + "_depth")

            # Saving Point Cloud
            # save_point_cloud = sl.save_camera_point_cloud_as(cam,
            #                                                  sl.POINT_CLOUD_FORMAT.
            #                                                  POINT_CLOUD_FORMAT_PCD_ASCII,
            #                                                  filepath, True)
            frame_count +=1
            # saving_image(key, mat)
        else:
            print("Error breaking")
            break
    cv2.destroyAllWindows()

    print_camera_information(cam)

    cam.close()
    print("\nFINISH")

if __name__ == "__main__":
    main()