import numpy as np
import pyzed.sl as sl
from numpy import inf

scale = 3

def print_camera_information(cam):
    try:
        print("Resolution: {0}, {1}.".format(
            round(cam.get_resolution().width, 2), cam.get_resolution().height))
        print("Camera FPS: {0}.".format(cam.get_camera_fps()))
        print("Firmware: {0}.".format(
            cam.get_camera_information().firmware_version))
        print("Serial number: {0}.\n".format(
            cam.get_camera_information().serial_number))
    except Exception as e:
        print(e)


def load_image_into_numpy_array(image):
    try:
        ar = image.get_data()
        # print(ar.shape)
        ar = ar[:, :, 0:3]
        (im_height, im_width, channels) = image.get_data().shape
    except Exception as e:
        print("Errror", e)
        raise e
    return np.array(ar).reshape((im_height, im_width, 3)).astype(np.uint8)

def getParams():
    init = sl.InitParameters()
    init.coordinate_units = sl.UNIT.UNIT_METER
    init.camera_resolution = sl.RESOLUTION.RESOLUTION_HD2K
    init.depth_mode = sl.DEPTH_MODE.DEPTH_MODE_ULTRA

    init.svo_real_time_mode = False
    init.camera_fps = 30
    return init

def eliminate_values(x):
    x[x == inf] = 0
    x[x == -inf] = 0
    x = np.nan_to_num(x)
    return x


def run_zed_camera():
    cam = None
    try:
        print("Running...")
        init = getParams()

        cam = sl.Camera()
        status = cam.open(init)
        if not cam.is_opened():
            print("Opening ZED Camera...")
        if status != sl.ERROR_CODE.SUCCESS:
            print(repr(status))
            exit()

        # cam.set_depth_max_range_value(40)
        cam.set_depth_max_range_value(40)
        runtime = sl.RuntimeParameters()
        runtime.sensing_mode = sl.SENSING_MODE.SENSING_MODE_STANDARD

        print_camera_information(cam)
        return cam, runtime

    except Exception as e:
        if cam is not None:
            cam.close()
        print("Error", e)